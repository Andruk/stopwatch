import React, {useState, useRef} from 'react';
import Button from "../../shared/components/Button";

const Display = () => {
    const [time, setTime] = useState({
        seconds:0,
        minutes:0,
        hours:0})
    const [status, setStatus] = useState(true)
    const intervalRef = useRef

    let updatedSeconds = time.seconds
    let updatedMinutes = time.minutes
    let updatedHours = time.hours

    const start = () => {
        run();
        setStatus(false)
        intervalRef.current = setInterval(run, 1000)
    };

    const stop = () => {
        clearInterval(intervalRef.current)
        setTime({
            seconds:0,
            minutes:0,
            hours:0})
        setStatus(true)
    };
    const reset = () => {
        setTime({
            seconds:0,
            minutes:0,
            hours:0})
    }

    const wait = () => {
        clearInterval(intervalRef.current)
        setStatus(true)
    }

    const run = () => {
        if(updatedMinutes === 60){
            updatedHours++
            updatedMinutes = 0
        }
        if(updatedSeconds === 60){
            updatedMinutes++
            updatedSeconds = 0
        }
        updatedSeconds++
        return setTime({seconds:updatedSeconds, minutes: updatedMinutes, hours: updatedHours})
    }

     return (
            <>
            <div>
                <span>{time.hours}</span>&nbsp;:&nbsp;
                <span>{time.minutes >= 10? time.minutes : "0"+ time.minutes}</span>&nbsp;:&nbsp;
                <span>{time.seconds >= 10? time.seconds : "0"+ time.seconds}</span>&nbsp;
            </div>
                {status ? <Button onClick={start} className="start"  >Start</Button> :
                    <Button onClick={stop} className="stop">Stop</Button>}
                <Button onClick={wait} className="wait" >Wait</Button>
                <Button onClick={reset} >Reset</Button>
            </>
        );
}

export default Display;