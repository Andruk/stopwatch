import './App.css';

import Display from "./client/StopWatchDisplay";

function App() {
  return (
    <div className="App">
      <Display />

    </div>
  );
}

export default App;
