import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
    const{children, onClick, className} = props
    const fullClassName = `btn ${className}`
        return (
            <button className={fullClassName} onClick={onClick}>{children}</button>
        );

}

Button.propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func,
    className: PropTypes.string,
}

Button.defaultProps ={
    children: 'Default Btn',
    className: '',
}
export default Button;